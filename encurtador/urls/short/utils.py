import string
import random

#from short.models import import Urls
from django.conf import settings

SHORTCODE_MIN = getattr(settings, "SHORTCODE_MIN", 6)

def code_generetor(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def create_shortcode(instance, size=SHORTCODE_MIN):
    codigo = code_generetor(size=size)
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(urlcurta=codigo).exists()
    if qs_exists:
        return create_shortcode(size=size)
    return codigo