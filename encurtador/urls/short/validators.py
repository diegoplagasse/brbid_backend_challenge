from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator

def validade_url(value):
    url_validator = URLValidator()
    value_1_invalid = False
    value_2_invalid = False

    try:
        url_validator(value)
    except:
        value_1_invalid = True

    value_2_url = "http://" + value

    try:
        url_validator(value_2_url)
    except:
        value_2_invalid = True

    if value_1_invalid == False and value_2_invalid == False:
        raise ValidationError("URL inválida")

    return value

def validate_com(value):
    if not "com" in value:
        raise ValidationError("Esse link não é válido pois não possui .COM")
    return value
