from django.shortcuts import render, get_object_or_404, redirect
from django.template import context
from django.contrib.sessions.models import Session
from .models import Urls
from .forms import UrlsForm, UserForm, FormularioURL
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View


# Create your views here.
def index(request):
    if not request.user.is_authenticated():
        return render(request, 'short/home.html')
    else:
        urls = Urls.objects.filter(user=request.user)
        return render(request, 'short/index.html', {'urls': urls})


def detail(request, url_id):
    url = get_object_or_404(Urls, pk=url_id)
    return render(request, 'short/detail.html', {'url': url})


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                urls = Urls.objects.filter(user=request.user)
                return render(request, 'short/index.html', {'urls': urls})
            else:
                return render(request, 'short/login.html', {'error_message': 'Sua conta não está ativa.'})
        else:
            return render(request, 'short/login.html', {'error_message': 'Login inválido ou inexistente'})
    return render(request, 'short/login.html')


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                urls = Urls.objects.filter(user=request.user)
                return render(request, 'short/index.html', {'urls': urls})
    context = {
        "form": form,
    }
    return render(request, 'short/register.html', context)


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'short/login.html', context)


def create_url(request):

        form = UrlsForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            url = form.save(commit=False)
            url.user = request.user
            return UrlsForm(request, 'short/detail.html', {'url': url})
        context = {
            "form": form,
        }
        return render(request, 'short/create_url.html', context)

def delete_url(request, url_id):
    url = Urls.objects.get(pk=url_id)
    url.delete()
    urls = Urls.objects.filter(user=request.user)
    return render(request, 'short/index.html', {'urls': urls})


def url_redirect_view(request, urlcurta=None, *args, **kwargs):
    obj = get_object_or_404(request, urlcurta=None)
    return HttpResponse("URL: {sc}".format(sc=obj.urlnormal))


def url_view_fbv(request, *args, **kwargs):
    if request.method == "POST":
        print(request.POST)
    return render(request, "short/nova_url.html", {})

class UrlView(View):
    def get(self, request, *args, **kwargs):
        form = FormularioURL()
        context = {
            "title" : "Encurtar URL",
            "form" : form
        }
        return render(request, 'short/nova_url.html', context)

    def post(self, request, *args, **kwargs):

        form = FormularioURL(request.POST)
        context = {
            "title": "Encurtar URL",
            "form": form
        }

        template = "short/nova_url.html"

        if form.is_valid():
            user = request.user
            titulo_url = form.cleaned_data['titulo_url']
            new_url = form.cleaned_data['urlnormal']
            obj, created = Urls.objects.get_or_create(urlnormal=new_url, titulo_url=titulo_url)
            context = {
                "object": obj,
                "created": created,
            }

            if created:
                template = "short/sucesso.html"
            else:
                template = "short/existe.html"


        return render(request, template , context)

class UrlCBView(View):
    def get(self, request, urlcurta=None, *args, **kwargs):
        obj = get_object_or_404(Urls, urlcurta=urlcurta)
        return HttpResponseRedirect(obj.urlnormal)

def redirect_root(request):
    return redirect('/short/')