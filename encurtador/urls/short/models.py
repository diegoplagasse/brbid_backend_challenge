from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Permission, User
from django.conf import settings
from .validators import validade_url, validate_com
from .utils import code_generetor, create_shortcode


SHORTCODE_MAX = getattr(settings, "SHORTCODE_MAX", 15)

class UrlManager(models.Manager):
    def all(self, *args, **kwargs):
        qs_main = super(UrlManager, self).all(*args, **kwargs)
        qs = qs_main.filter(active=True)
        return qs


    def refresh_shortcodes(self, new_code=None):
        qs = Urls.objects.filter(id__gte=1)
        new_code = 0
        for q in qs:
            q.urlcurta = create_shortcode(q)
            print(q.urlcurta)
            q.save()
            new_code += 1
        return "New Codes: {i}".format(i=new_code)

class Urls(models.Model):
    titulo_url = models.CharField(max_length=100, null=True, blank=True)
    urlnormal = models.CharField(max_length=10000, unique=False, validators=[validade_url,  validate_com])
    urlcurta = models.CharField(max_length=SHORTCODE_MAX, unique=True, blank=True)
    user = models.ForeignKey(User, default=0,  null=True)

    objects = UrlManager()

    def save(self, *args, **kwargs):
        if self.urlcurta is None or self.urlcurta == "":
            self.urlcurta = create_shortcode(self)
        super(Urls, self).save(*args, **kwargs)

    def __str__(self):
        return self.urlnormal








