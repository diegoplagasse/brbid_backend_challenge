from django.conf.urls import url
from django.contrib import admin


from . import views


app_name = 'short'

urlpatterns = [
    url(r'^$', views.index, name='index'),

    #shorts/<url_id>/
    url(r'^(?P<url_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login_user/$', views.login_user, name='login_user'),
    url(r'^logout_user/$', views.logout_user, name='logout_user'),
    url(r'^create_url/$', views.create_url, name='create_url'),
    url(r'^(?P<urlcurta>[\W-]+){6,15}/$', views.UrlCBView.as_view, name='urlcurta' ),

    url(r'^nova/$', views.UrlView.as_view(), name='nova'),
]