from django import forms
from django.contrib.auth.models import User

from .models import Urls
from .validators import validade_url, validate_com



class FormularioURL(forms.Form):
    titulo_url = forms.CharField(label='Título URL')
    urlnormal = forms.CharField(label='Encurtar URL', validators=[validade_url, validate_com])

    '''def clean(self):
        cleaned_data = super(FormularioURL, self). clean()
        print(cleaned_data)
        url = cleaned_data.get('urlnormal')
        url_validator = URLValidator()
        try:
            url_validator(url)
        except:
            raise forms.ValidationError("URL Inválida")
        print(url)
    
    def clean_url(self):
        url = self.cleaned_data['urlnormal']
        #print(url)

        url_validator = URLValidator()
        try:
            url_validator(url)
        except:
            raise forms.ValidationError("URL inválida")
        return url
    '''

class UrlsForm(forms.ModelForm):

    class Meta:
        model = Urls
        fields = ['titulo_url', 'urlnormal', 'urlcurta']

    def clean(self):
        cleaned_data = super(UrlsForm, self). clean()
        print(cleaned_data)


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
